#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Скрипт распарсивает формат json или xml со
статьями в подкаталоге texts и выводит 10
самых частых слов длиной более 6 символов
для каждого файла
При этом используется библиотека nltk,
с помощью которой производится удаление
стоп слов, стеммизация и построение корзины слов

"""

if __name__ == '__main__':
    
    import os
    import sys
    import json
    import xml.etree.ElementTree as ET
    import chardet
    from pprint import pprint
    from nltk.tokenize import word_tokenize
    from collections import Counter
    from nltk.stem.snowball import RussianStemmer as Stemmer
    from nltk.corpus import stopwords
    
    
    texts_dir = 'texts'
    
    script_dir = os.path.dirname(os.path.abspath(__file__))
    texts_dir = os.path.join(script_dir, texts_dir)
    
    def analise_text(text):
        """Анализ текста
        печать 10 наиболее часто встречающихся слов
        длиной более 6 символов
        
        """
        if text is None: return
        
        # приводим к нижнему регистру и токенизируем:
        text = word_tokenize(text.lower())
        
        # удаляем токены длиной более 6 символов, токены не из букв и
        # стоп слова
        text = [t for t in text if len(t) > 6
                                and t.isalpha()
                                and t not in stopwords.words('russian')]
        
        # делаем стеммизацию (не нашел лексический анализатор для русского)
        stemmer = Stemmer()
        text = [stemmer.stem(t) for t in text]
        
        # создаем корзину слов
        bow = Counter(text)
        
        # печатаем 10 наиболее употребимых
        pprint(bow.most_common(10))
        
        
    
    def parse_json(filename):
        """Распарсивает json и возвращает текстовое
        представление статей
        
        """
        with open(filename, 'br') as file:
            enc = chardet.detect(file.read(512))['encoding']
            
        with open(filename, encoding=enc, errors='ignore') as file:
            data = json.load(file)
            
        #pprint(data)
        
        all_articles = ''
        for entry in data['rss']['channel']['items']:
            all_articles += '\n\n' + entry['title'] \
                          + '\n\n' + entry['description']
            
        #print(all_articles)
        return all_articles
        
        
    
    def parse_xml(filename):
        """Распарсивает xml и возвращает текстовое
        представление статей
        
        """
        with open(filename, 'br') as file:
            enc = chardet.detect(file.read(512))['encoding']
            
        with open(filename, encoding=enc, errors='ignore') as file:
            data = file.read()
        
        all_articles = ''
        root = ET.fromstring(data)
        for item in root.find('channel').findall('item'):
            all_articles += '\n\n' + item.find('title').text \
                          + '\n\n' + item.find('description').text
        #print(all_articles)
        return all_articles
    
    
    def main():
        """Выбор типа распарсиваемых файлов пользователем и
        управление распарсиванием
        
        """
        print('Выберите тип распарсиваемых файлов:')
        print('1 - json')
        print('2 - xml')
        user_input = input(':').strip()
        #user_input = '2'
        
        if user_input == '1':
            print('\nОбработка файлов json')
            file_ext = '.json'
            parse_function = parse_json
        elif user_input == '2':
            print('\nОбработка файлов xml')
            file_ext = '.xml'
            parse_function = parse_xml
        else:
            print('Неподдерживаемая опция', file=sys.stderr)
            sys.exit(1)
            
        files = [file for file in os.listdir(texts_dir)
                 if file.lower().endswith(file_ext)]
        
        for entry in files:
            print('\nФайл:', entry)
            analise_text(parse_function(os.path.join(texts_dir, entry)))
    
    
    main()
            